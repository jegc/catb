Clone the repository and create the book in the desired format.

For example for create the book in epub format:

1. Install the dbtoepub package
1. Create the book:

```bash
dbtoepub catb.xml
```